package ru.fedun.tm.api.service;

import ru.fedun.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

    String[] getCommands();

    String[] getArgs();
}
