package ru.fedun.tm.controller;

import ru.fedun.tm.api.controller.ICrudController;
import ru.fedun.tm.api.service.ICrudService;
import ru.fedun.tm.model.Project;
import ru.fedun.tm.util.TerminalUtil;

import java.util.List;

public class ProjectController implements ICrudController<Project> {

    private final ICrudService<Project> projectService;

    public ProjectController(ICrudService<Project> projectService) {
        this.projectService = projectService;
    }

    public void showAll() {
        System.out.println("[PROJECT LIST]");
        final List<Project> projects = projectService.findAll();
        for (Project project : projects) {
            System.out.println(project);
        }
        System.out.println("[OK]");
        System.out.println();
    }

    public void clear() {
        System.out.println("[PROJECT CLEAR]");
        projectService.clear();
        System.out.println("[OK]");
        System.out.println();
    }

    public void create() {
        System.out.println("[PROJECT CREATE]");
        System.out.println("[ENTER PROJECT TITLE]");
        final String title = TerminalUtil.nextLine();
        System.out.println("[ENTER PROJECT DESCRIPTION]");
        final String description = TerminalUtil.nextLine();
        projectService.create(title, description);
        System.out.println("[OK]");
        System.out.println();
    }

    @Override
    public void showOneById() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("[ENTER ID:]");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.getOneById(id);
        if (project == null) System.out.println("[FAIL]");
        else showProject(project);
    }

    @Override
    public void showOneByIndex() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("[ENTER INDEX:]");
        final Integer index = TerminalUtil.nextInt() - 1;
        final Project project = projectService.getOneByIndex(index);
        if (project == null) System.out.println("[FAIL]");
        else showProject(project);
        System.out.println();
    }

    @Override
    public void showOneByTitle() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("[ENTER TITLE:]");
        final String title = TerminalUtil.nextLine();
        final Project project = projectService.getOneByTitle(title);
        if (project == null) System.out.println("[FAIL]");
        else showProject(project);
    }

    public void showProject(final Project project) {
        if (project == null) return;
        System.out.println("ID: " + project.getId());
        System.out.println("TITLE: " + project.getTitle());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("[OK]");
    }

    @Override
    public void updateOneByIndex() {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("[ENTER INDEX:]");
        final Integer index = TerminalUtil.nextInt();
        final Project project = projectService.getOneByIndex(index);
        if (project == null) {
            System.out.println("[FAIL]");
        }
        System.out.println("ENTER TITLE:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdated = projectService.updateByIndex(index, name, description);
        if (projectUpdated == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void updateOneById() {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("[ENTER ID:]");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.getOneById(id);
        if (project == null) {
            System.out.println("[FAIL]");
        }
        System.out.println("ENTER TITLE:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdated = projectService.updateById(id, name, description);
        if (projectUpdated == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void removeOneById() {
        System.out.println("[REMOVE PROJECT]");
        System.out.println("[ENTER ID:]");
        final String id = TerminalUtil.nextLine();
        if (id == null || id.isEmpty()) {
            System.out.println("[FAIL]");
            return;
        }
        final Project project = projectService.removeOneById(id);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void removeOneByIndex() {
        System.out.println("[REMOVE PROJECT]");
        System.out.println("[ENTER INDEX:]");
        final Integer index = TerminalUtil.nextInt();
        if (index <= 0) {
            System.out.println("[FAIL]");
            return;
        }
        final Project project = projectService.removeOneByIndex(index);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void removeOneByTitle() {
        System.out.println("[REMOVE PROJECT]");
        System.out.println("[ENTER TITLE:]");
        final String title = TerminalUtil.nextLine();
        if (title == null || title.isEmpty()) {
            System.out.println("[FAIL]");
            return;
        }
        final Project project = projectService.removeOneByTitle(title);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

}
