package ru.fedun.tm.exception.empty;

import ru.fedun.tm.exception.AbstractRuntimeException;

public class EmptyCommandException extends AbstractRuntimeException {

    private final static String message = "Error! Empty command...";

    public EmptyCommandException() {
        super(message);
    }

}
