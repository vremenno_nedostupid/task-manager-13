package ru.fedun.tm.exception.notfound;

import ru.fedun.tm.exception.AbstractRuntimeException;

public class ProjectNotFound extends AbstractRuntimeException {

    private final static String message = "Error! Project not found...";

    public ProjectNotFound() {
        super(message);
    }
}
