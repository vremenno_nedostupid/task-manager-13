package ru.fedun.tm.repository;

import ru.fedun.tm.api.repository.ICrudRepository;
import ru.fedun.tm.exception.notfound.TaskNotFoundException;
import ru.fedun.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository implements ICrudRepository<Task> {

    private final List<Task> tasks = new ArrayList<>();

    public List<Task> findAll() {
        return tasks;
    }

    @Override
    public void clear() {
        tasks.clear();
    }

    @Override
    public void add(Task task) {
        tasks.add(task);
    }

    @Override
    public void remove(Task task) {
        tasks.remove(task);
    }

    @Override
    public Task findOneById(final String id) {
        for (final Task task : tasks) {
            if (id.equals(task.getId())) return task;
        }
        throw new TaskNotFoundException();
    }

    @Override
    public Task findOneByIndex(final Integer index) {
        return tasks.get(index);
    }

    @Override
    public Task findOneByTitle(final String title) {
        for (final Task task : tasks) {
            if (title.equals(task.getTitle())) return task;
        }
        throw new TaskNotFoundException();
    }

    @Override
    public Task removeOneByIndex(final Integer index) {
        final Task task = findOneByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        remove(task);
        return task;
    }

    @Override
    public Task removeOneByTitle(final String title) {
        final Task task = findOneByTitle(title);
        if (task == null) throw new TaskNotFoundException();
        remove(task);
        return task;
    }

    @Override
    public Task removeOneById(final String id) {
        final Task task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        remove(task);
        return task;
    }

}
