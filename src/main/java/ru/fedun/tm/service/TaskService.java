package ru.fedun.tm.service;

import ru.fedun.tm.api.repository.ICrudRepository;
import ru.fedun.tm.api.service.ICrudService;
import ru.fedun.tm.exception.empty.EmptyIdException;
import ru.fedun.tm.exception.empty.EmptyTitleException;
import ru.fedun.tm.exception.incorrect.IncorrectIndexException;
import ru.fedun.tm.exception.notfound.TaskNotFoundException;
import ru.fedun.tm.model.Task;

import java.util.List;

public class TaskService implements ICrudService<Task> {

    private final ICrudRepository<Task> taskRepository;

    public TaskService(final ICrudRepository<Task> taskRepository) {
        this.taskRepository = taskRepository;
    }

    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    public void clear() {
        taskRepository.clear();
    }

    @Override
    public void create(final String title) {
        if (title == null || title.isEmpty()) throw new EmptyTitleException();
        final Task task = new Task();
        task.setTitle(title);
        taskRepository.add(task);
    }

    @Override
    public void create(final String title, final String description) {
        if (title == null || title.isEmpty()) throw new EmptyTitleException();
        final Task task = new Task();
        task.setTitle(title);
        task.setDescription(description);
        taskRepository.add(task);
    }

    @Override
    public Task getOneById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyTitleException();
        return taskRepository.findOneById(id);
    }

    @Override
    public Task getOneByIndex(final Integer index) {
        if (index == null || index < 0) throw new IncorrectIndexException();
        return taskRepository.findOneByIndex(index);
    }

    @Override
    public Task getOneByTitle(final String title) {
        if (title == null || title.isEmpty()) throw new EmptyTitleException();
        return taskRepository.findOneByTitle(title);
    }

    @Override
    public Task updateById(final String id, final String title, final String description) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (title == null || title.isEmpty()) throw new EmptyTitleException();
        final Task task = taskRepository.findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setId(id);
        task.setTitle(title);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateByIndex(final Integer index, final String title, final String description) {
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (title == null || title.isEmpty()) throw new EmptyTitleException();
        final Task task = taskRepository.findOneByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        task.setTitle(title);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task removeOneById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return taskRepository.removeOneById(id);
    }

    @Override
    public Task removeOneByIndex(final Integer index) {
        if (index == null || index < 0) throw new IncorrectIndexException();
        return taskRepository.removeOneByIndex(index);
    }

    @Override
    public Task removeOneByTitle(final String title) {
        if (title == null || title.isEmpty()) throw new EmptyTitleException();
        return taskRepository.removeOneByTitle(title);
    }

}
